package fr.dawan.springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.dawan.springboot.dto.ArticleDto;
import fr.dawan.springboot.services.ArticleService;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {

    @Autowired
    ArticleService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ArticleDto> getAllArticle(Pageable page) {
        return service.getAll(page);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto getArticleById(@PathVariable long id) {
        return service.getById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto addArticle(@RequestBody ArticleDto article) {
        return service.saveOrUpdate(article);
    }
    
    @PostMapping(value="/save-image/{id}", consumes="multipart/form-data", produces="text/plain")
    public ResponseEntity<String> uploadImage(@PathVariable long id, @RequestParam("file") MultipartFile file) throws Exception{
        ArticleDto arDto = service.getById(id);
        arDto.setImage(file.getBytes());
        service.saveOrUpdate(arDto);
        return ResponseEntity.ok("Upload done !");
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ArticleDto updateArticle(@PathVariable long id, @RequestBody ArticleDto article) {
        return service.saveOrUpdate(article);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> delete(@PathVariable long id) {
        try {
            service.delete(id);
            return ResponseEntity.status(HttpStatus.CREATED).body("L'article id=" + id + " est supprimé");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("L'article id=" + id + " n'existe pas");

        }
    }
}
