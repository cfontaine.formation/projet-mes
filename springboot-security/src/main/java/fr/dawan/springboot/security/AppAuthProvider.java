package fr.dawan.springboot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import fr.dawan.springboot.services.UserServiceImpl;

public class AppAuthProvider extends DaoAuthenticationProvider {

    @Autowired
    private UserServiceImpl userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
      String userName=authentication.getName();
      String password=authentication.getCredentials().toString();
      UserDetails user=userService.loadUserByUsername(userName);
      if(password!=null && password.equals(user.getPassword())) {
          return new UsernamePasswordAuthenticationToken(user,null,user.getAuthorities());
      }
      else {
          throw new BadCredentialsException("Mauvais mot de passe");
      }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }

    
}
