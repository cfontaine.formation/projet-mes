package fr.dawan.springboot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private UserDetailsService userService;
    
    @Bean
    public AuthenticationProvider authProvider() {
        AppAuthProvider provider = new AppAuthProvider();
        provider.setUserDetailsService(userService);
        return provider;
    }

    @Bean
    public AuthenticationManager authManager(HttpSecurity http,AuthenticationProvider authProvider) throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder = 
            http.getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder.authenticationProvider(authProvider);
        return authenticationManagerBuilder.build();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.
        csrf().disable().
        authorizeHttpRequests(authorize -> authorize
                .requestMatchers("/api/users/**").hasAnyAuthority("ADMIN")
                .requestMatchers(HttpMethod.POST).hasAnyAuthority("WRITE","ADMIN")
                .requestMatchers(HttpMethod.PUT).hasAnyAuthority("WRITE","ADMIN")
                .requestMatchers(HttpMethod.DELETE).hasAnyAuthority("DELETE","ADMIN")
                .anyRequest().permitAll())
            .httpBasic();
        return http.build();
    }
}
