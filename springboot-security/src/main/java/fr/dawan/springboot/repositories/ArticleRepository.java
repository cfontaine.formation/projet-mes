package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springboot.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    
    List<Article> findByDescriptionLike(String model);
}
