package fr.dawan.springboot.services;

import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.dawan.springboot.dto.UserDto;
import fr.dawan.springboot.entities.User;
import fr.dawan.springboot.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private ModelMapper mapper;

    public UserServiceImpl(UserRepository useRepository, ModelMapper mapper) {
        this.userRepository = useRepository;
        this.mapper = mapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("Utilisateur non trouvé")); // TODO message à modifier
    }

    @Override
    public Page<UserDto> getAll(Pageable page) {
        Page<User> pageUser = userRepository.findAll(page);
        return new PageImpl<>(pageUser.get().map(a -> mapper.map(a, UserDto.class)).collect(Collectors.toList()), page,
                pageUser.getTotalElements());
    }

    @Override
    public UserDto getById(long id) {
        return mapper.map(userRepository.findById(id).get(), UserDto.class);
    }

    @Override
    public UserDto saveOrUpdate(UserDto uDto) {

        User u = userRepository.saveAndFlush(mapper.map(uDto, User.class));
        return mapper.map(u, UserDto.class);
    }

    @Override
    public void delete(long id) {
        userRepository.deleteById(id);
    }

}
