package fr.dawan.springboot.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import fr.dawan.springboot.dto.UserDto;

public interface UserService extends UserDetailsService {

    Page<UserDto> getAll(Pageable page);

    UserDto getById(long id);

    UserDto saveOrUpdate(UserDto uDto);

    void delete(long id);
}
