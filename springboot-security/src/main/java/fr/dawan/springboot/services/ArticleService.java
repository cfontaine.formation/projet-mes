package fr.dawan.springboot.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dto.ArticleDto;

public interface ArticleService {

    List<ArticleDto> getAll(Pageable page);

    ArticleDto getById(long id);

    void delete(long id);

    List<ArticleDto> getByDescription(String description);
    
    ArticleDto saveOrUpdate(ArticleDto article);
}
