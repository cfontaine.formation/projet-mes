package fr.dawan.springboot.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dto.ArticleDto;
import fr.dawan.springboot.entities.Article;
import fr.dawan.springboot.repositories.ArticleRepository;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService{

    @Autowired
    private ArticleRepository repository;
    
    @Autowired
    private ModelMapper mapper;
    
    public List<ArticleDto> getAll(Pageable page){
        return repository.findAll(page).stream().map(a -> mapper.map(a, ArticleDto.class)).collect(Collectors.toList());
    }
    
    public ArticleDto getById(long id) {
        return mapper.map(repository.findById(id).get(), ArticleDto.class);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
        
    }

    @Override
    public List<ArticleDto> getByDescription(String description) {
        return repository.findByDescriptionLike("%" + description + "%").stream().map(a -> mapper.map(a, ArticleDto.class)).collect(Collectors.toList());
    }

    @Override
    public ArticleDto saveOrUpdate(ArticleDto article) {
        Article a=repository.saveAndFlush(mapper.map(article,Article.class));
        return mapper.map(a,ArticleDto.class);
    }
    
}

