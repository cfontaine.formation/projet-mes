package fr.dawan.springboot.dto;

public class UserDto {

    private long id;

    private int version;

    private String email;

    public UserDto() {

    }

    public UserDto(long id, int version, String email) {
        this.id = id;
        this.version = version;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}