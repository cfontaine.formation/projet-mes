package fr.dawan.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloWorldController {

    @GetMapping("/hello")
    public String helloworld(Model model) {
        model.addAttribute("message", "Salut!");
        System.out.println("aaaaa");
        return "helloword";
    }
}
